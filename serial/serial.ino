long randNumber;

void setup() {
  Serial.begin(9600);

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));
}

void loop() {
  // print a random number from 0 to 299
  randNumber = random(100);
  String strHeader = "S S ";
  String strFooter = " Kg";
  String strData = strHeader + randNumber + strFooter;
  Serial.println(strData);

  // print a random number from 10 to 19
  // randNumber = random(10, 20);
  // Serial.println(randNumber);

  delay(500);
}
